// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_2GameMode.generated.h"

UCLASS(minimalapi)
class ATDS_2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_2GameMode();
};



