// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_2, "TDS_2" );

DEFINE_LOG_CATEGORY(LogTDS_2)
 