// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_2GameMode.h"
#include "TDS_2PlayerController.h"
#include "TDS_2Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS_2GameMode::ATDS_2GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_2PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
