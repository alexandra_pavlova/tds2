// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TDS_2/TDS_2GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTDS_2GameMode() {}
// Cross Module References
	TDS_2_API UClass* Z_Construct_UClass_ATDS_2GameMode_NoRegister();
	TDS_2_API UClass* Z_Construct_UClass_ATDS_2GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_TDS_2();
// End Cross Module References
	void ATDS_2GameMode::StaticRegisterNativesATDS_2GameMode()
	{
	}
	UClass* Z_Construct_UClass_ATDS_2GameMode_NoRegister()
	{
		return ATDS_2GameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATDS_2GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATDS_2GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TDS_2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDS_2GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TDS_2GameMode.h" },
		{ "ModuleRelativePath", "TDS_2GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATDS_2GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATDS_2GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATDS_2GameMode_Statics::ClassParams = {
		&ATDS_2GameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATDS_2GameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATDS_2GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATDS_2GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATDS_2GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATDS_2GameMode, 1749594272);
	template<> TDS_2_API UClass* StaticClass<ATDS_2GameMode>()
	{
		return ATDS_2GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATDS_2GameMode(Z_Construct_UClass_ATDS_2GameMode, &ATDS_2GameMode::StaticClass, TEXT("/Script/TDS_2"), TEXT("ATDS_2GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATDS_2GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
