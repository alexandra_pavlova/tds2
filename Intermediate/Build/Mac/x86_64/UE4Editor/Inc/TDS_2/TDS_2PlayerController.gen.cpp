// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TDS_2/TDS_2PlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTDS_2PlayerController() {}
// Cross Module References
	TDS_2_API UClass* Z_Construct_UClass_ATDS_2PlayerController_NoRegister();
	TDS_2_API UClass* Z_Construct_UClass_ATDS_2PlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_TDS_2();
// End Cross Module References
	void ATDS_2PlayerController::StaticRegisterNativesATDS_2PlayerController()
	{
	}
	UClass* Z_Construct_UClass_ATDS_2PlayerController_NoRegister()
	{
		return ATDS_2PlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ATDS_2PlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATDS_2PlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_TDS_2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDS_2PlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TDS_2PlayerController.h" },
		{ "ModuleRelativePath", "TDS_2PlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATDS_2PlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATDS_2PlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATDS_2PlayerController_Statics::ClassParams = {
		&ATDS_2PlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATDS_2PlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATDS_2PlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATDS_2PlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATDS_2PlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATDS_2PlayerController, 418616898);
	template<> TDS_2_API UClass* StaticClass<ATDS_2PlayerController>()
	{
		return ATDS_2PlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATDS_2PlayerController(Z_Construct_UClass_ATDS_2PlayerController, &ATDS_2PlayerController::StaticClass, TEXT("/Script/TDS_2"), TEXT("ATDS_2PlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATDS_2PlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
