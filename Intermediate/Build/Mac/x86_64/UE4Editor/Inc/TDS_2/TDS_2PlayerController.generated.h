// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TDS_2_TDS_2PlayerController_generated_h
#error "TDS_2PlayerController.generated.h already included, missing '#pragma once' in TDS_2PlayerController.h"
#endif
#define TDS_2_TDS_2PlayerController_generated_h

#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_SPARSE_DATA
#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_RPC_WRAPPERS
#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATDS_2PlayerController(); \
	friend struct Z_Construct_UClass_ATDS_2PlayerController_Statics; \
public: \
	DECLARE_CLASS(ATDS_2PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS_2"), NO_API) \
	DECLARE_SERIALIZER(ATDS_2PlayerController)


#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATDS_2PlayerController(); \
	friend struct Z_Construct_UClass_ATDS_2PlayerController_Statics; \
public: \
	DECLARE_CLASS(ATDS_2PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS_2"), NO_API) \
	DECLARE_SERIALIZER(ATDS_2PlayerController)


#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATDS_2PlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATDS_2PlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDS_2PlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDS_2PlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDS_2PlayerController(ATDS_2PlayerController&&); \
	NO_API ATDS_2PlayerController(const ATDS_2PlayerController&); \
public:


#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDS_2PlayerController(ATDS_2PlayerController&&); \
	NO_API ATDS_2PlayerController(const ATDS_2PlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDS_2PlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDS_2PlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATDS_2PlayerController)


#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_9_PROLOG
#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_SPARSE_DATA \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_RPC_WRAPPERS \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_INCLASS \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_SPARSE_DATA \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_INCLASS_NO_PURE_DECLS \
	TDS_2_Source_TDS_2_TDS_2PlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_2_API UClass* StaticClass<class ATDS_2PlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TDS_2_Source_TDS_2_TDS_2PlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
