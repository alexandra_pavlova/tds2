// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TDS_2_TDS_2GameMode_generated_h
#error "TDS_2GameMode.generated.h already included, missing '#pragma once' in TDS_2GameMode.h"
#endif
#define TDS_2_TDS_2GameMode_generated_h

#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_SPARSE_DATA
#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_RPC_WRAPPERS
#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATDS_2GameMode(); \
	friend struct Z_Construct_UClass_ATDS_2GameMode_Statics; \
public: \
	DECLARE_CLASS(ATDS_2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS_2"), TDS_2_API) \
	DECLARE_SERIALIZER(ATDS_2GameMode)


#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATDS_2GameMode(); \
	friend struct Z_Construct_UClass_ATDS_2GameMode_Statics; \
public: \
	DECLARE_CLASS(ATDS_2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS_2"), TDS_2_API) \
	DECLARE_SERIALIZER(ATDS_2GameMode)


#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TDS_2_API ATDS_2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATDS_2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TDS_2_API, ATDS_2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDS_2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TDS_2_API ATDS_2GameMode(ATDS_2GameMode&&); \
	TDS_2_API ATDS_2GameMode(const ATDS_2GameMode&); \
public:


#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TDS_2_API ATDS_2GameMode(ATDS_2GameMode&&); \
	TDS_2_API ATDS_2GameMode(const ATDS_2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TDS_2_API, ATDS_2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDS_2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATDS_2GameMode)


#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define TDS_2_Source_TDS_2_TDS_2GameMode_h_9_PROLOG
#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_SPARSE_DATA \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_RPC_WRAPPERS \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_INCLASS \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TDS_2_Source_TDS_2_TDS_2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_SPARSE_DATA \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	TDS_2_Source_TDS_2_TDS_2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_2_API UClass* StaticClass<class ATDS_2GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TDS_2_Source_TDS_2_TDS_2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
