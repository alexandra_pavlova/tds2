// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TDS_2_TDS_2Character_generated_h
#error "TDS_2Character.generated.h already included, missing '#pragma once' in TDS_2Character.h"
#endif
#define TDS_2_TDS_2Character_generated_h

#define TDS_2_Source_TDS_2_TDS_2Character_h_12_SPARSE_DATA
#define TDS_2_Source_TDS_2_TDS_2Character_h_12_RPC_WRAPPERS
#define TDS_2_Source_TDS_2_TDS_2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TDS_2_Source_TDS_2_TDS_2Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATDS_2Character(); \
	friend struct Z_Construct_UClass_ATDS_2Character_Statics; \
public: \
	DECLARE_CLASS(ATDS_2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS_2"), NO_API) \
	DECLARE_SERIALIZER(ATDS_2Character)


#define TDS_2_Source_TDS_2_TDS_2Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATDS_2Character(); \
	friend struct Z_Construct_UClass_ATDS_2Character_Statics; \
public: \
	DECLARE_CLASS(ATDS_2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS_2"), NO_API) \
	DECLARE_SERIALIZER(ATDS_2Character)


#define TDS_2_Source_TDS_2_TDS_2Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATDS_2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATDS_2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDS_2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDS_2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDS_2Character(ATDS_2Character&&); \
	NO_API ATDS_2Character(const ATDS_2Character&); \
public:


#define TDS_2_Source_TDS_2_TDS_2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDS_2Character(ATDS_2Character&&); \
	NO_API ATDS_2Character(const ATDS_2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDS_2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDS_2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATDS_2Character)


#define TDS_2_Source_TDS_2_TDS_2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ATDS_2Character, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATDS_2Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(ATDS_2Character, CursorToWorld); }


#define TDS_2_Source_TDS_2_TDS_2Character_h_9_PROLOG
#define TDS_2_Source_TDS_2_TDS_2Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_SPARSE_DATA \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_RPC_WRAPPERS \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_INCLASS \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TDS_2_Source_TDS_2_TDS_2Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_SPARSE_DATA \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_INCLASS_NO_PURE_DECLS \
	TDS_2_Source_TDS_2_TDS_2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_2_API UClass* StaticClass<class ATDS_2Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TDS_2_Source_TDS_2_TDS_2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
